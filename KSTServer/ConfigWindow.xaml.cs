﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Net;

namespace KSTServer
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    public partial class ConfigWindow : Window
    {
        public ConfigWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Load the setting from config file
            Setting setting = Setting.LoadSettings("KSTServer.ini");

            //Show the setting
            ShowSetting(setting);

            //Save the setting to the config file (used to save a new file if the sonfig file had errors)
            SaveSetting();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (!ValidateParameters()) return;
            SaveSetting();
            Close();
        }
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Show the setting parameters inside the window
        /// </summary>
        /// <param name="setting">setting values</param>
        private void ShowSetting(Setting setting)
        {
            intBox0.Text = setting.Fps.ToString();
            intBox1.Text = setting.SensorDegree.ToString();
            intBox2.Text = setting.UdpPort.ToString();
            intBox3.Text = setting.TcpPort.ToString();
            floatBox4.Text = setting.Smoothing.ToString();
            floatBox5.Text = setting.Correction.ToString();
            floatBox6.Text = setting.Prediction.ToString();
            floatBox7.Text = setting.JitterRadius.ToString();
            floatBox8.Text = setting.MaxDeviationRadius.ToString();
            intBox9.Text = setting.ConfidenceWindow.ToString();
        }

        /// <summary>
        /// Save the setting to the config file
        /// </summary>
        private void SaveSetting()
        {
            StreamWriter writer = new StreamWriter("KSTServer.ini", false);
            writer.WriteLine("FPS " + intBox0.Text);
            writer.WriteLine("KinectSensorDegree " + intBox1.Text);
            writer.WriteLine("UDPPort " + intBox2.Text);
            writer.WriteLine("TCPPort " + intBox3.Text);
            writer.WriteLine("Smoothing " + floatBox4.Text);
            writer.WriteLine("Correction " + floatBox5.Text);
            writer.WriteLine("Prediction " + floatBox6.Text);
            writer.WriteLine("JitterRadius " + floatBox7.Text);
            writer.WriteLine("MaxDeviationRadius " + floatBox8.Text);
            writer.WriteLine("ConfidenceWindow " + intBox9.Text);
            writer.Close();
        }

        public bool ValidateIntParameter(string name, string value, int minValue, int maxValue)
        {
            int valid;
            if (int.TryParse(value, out valid))
            {
                if (valid < minValue || valid > maxValue)
                {
                    MessageBox.Show(name + " parameter must be in the range between " + minValue + " and " + maxValue);
                    return false;
                }
            }
            else
            {
                MessageBox.Show(name + " parameter must be an integer (Example: 12000).");
                return false;
            }

            return true;
        }
        public bool ValidateFloatParameter(string name, string value, float minValue, float maxValue)
        {
            float valid;
            if (float.TryParse(value, out valid))
            {
                if (valid < minValue || valid > maxValue)
                {
                    MessageBox.Show(name + " parameter must be in the range between " + minValue + " and " + maxValue);
                    return false;
                }
            }
            else
            {
                MessageBox.Show(name + " parameter must be a number (Example: 0.54).");
                return false;
            }

            return true;
        }
        private bool ValidateNumberParameter(string name, string value, float minValue)
        {
            float valid;
            if (float.TryParse(value, out valid))
            {
                if (valid < minValue)
                {
                    MessageBox.Show(name + " parameter must be greater than or equal to " + minValue);
                    return false;
                }
            }
            else
            {
                MessageBox.Show(name + " parameter must be a number (Example: 0.54).");
                return false;
            }

            return true;
        }

        public bool ValidateParameters()
        {
            return ValidateIntParameter("FPS", intBox0.Text, 0, 120)
                && ValidateIntParameter("Sensor Angle", intBox1.Text, -27, 27)
                && ValidateIntParameter("UDPPort", intBox2.Text, IPEndPoint.MinPort, IPEndPoint.MaxPort)
                && ValidateIntParameter("TCPPort", intBox3.Text, IPEndPoint.MinPort, IPEndPoint.MaxPort)
                && ValidateFloatParameter("Smoothing", floatBox4.Text, 0, 1)
                && ValidateFloatParameter("Correction", floatBox5.Text, 0, 1)
                && ValidateNumberParameter("Prediction", floatBox6.Text, 0)
                && ValidateNumberParameter("JitterRadius", floatBox7.Text, 0)
                && ValidateNumberParameter("MaxDeviationRadius", floatBox8.Text, 0);
        }
    }
}

