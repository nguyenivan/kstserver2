﻿#define APPEND_OUTPUT

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using Microsoft.Kinect;
using System.IO;
using System.Threading;
using Microsoft.Kinect.Face;
namespace KSTServer
{
    public class NetworkController
    {
        /// <summary>
        /// TCP connection
        /// </summary>
        private TcpListener tcpServer;

        /// <summary>
        /// List of tcp Clients
        /// </summary>
        private List<TcpClient> tcpClients = new List<TcpClient>();

        /// <summary>
        /// UDP Connection
        /// </summary>
        private UdpClient udpServer;

        /// <summary>
        /// Ports
        /// </summary>
        public int TcpPort { get; set; }
        public int UdpPort { get; set; }

        /// <summary>
        /// Confidence Window in milliseconds
        /// </summary>
        public int ConfidenceWindow { get; set; }


        /// <summary>
        /// Save the Framerate of data (FPS)
        /// </summary>
        public int FPS { get; set; }

        /// <summary>
        /// Id of the packet
        /// </summary>
        //private int idPacket = 0;

        /// <summary>
        /// Used to control the Report from different threads
        /// </summary>
        private object trafficLight = new object();

        /// <summary>
        /// Indicates when the server is stopped
        /// </summary>
        private bool stopped;

        /// <summary>
        /// Time of the last frame. Used to preserve the FPS
        /// </summary>
        int lastFrame;

        /// <summary>
        /// Initializes a new instance of the NetworkController class that use the specified ports.
        /// </summary>
        /// <param name="udpPort">Port used to sent broadcast udp packets</param>
        /// <param name="tcpPort">Port used to create tcp connections</param>
        public NetworkController(int udpPort, int tcpPort, int confidenceWindow)
        {
            if (File.Exists("KSTSkeletons.txt"))
                File.Delete("KSTSkeletons.txt");

            //Create the Tcp server
            tcpServer = new TcpListener(IPAddress.Any, tcpPort);
            tcpServer.Start();
            //Accept connections asyncronously.
            tcpServer.BeginAcceptTcpClient(AcceptClient, tcpServer);

            //Create the Udp Server
            udpServer = new UdpClient();
            this.UdpPort = udpPort;
            this.TcpPort = tcpPort;
            this.ConfidenceWindow = confidenceWindow;
        }

        public void Stop()
        {
            stopped = true;
            tcpServer.Server.Close();
            udpServer.Close();
        }

        /// <summary>
        /// Callback function to accept clients asyncronously
        /// </summary>
        /// <param name="ar">Status of the operation</param>
        public void AcceptClient(IAsyncResult ar)
        {
            if (stopped) return;
            //Get the server sent by the main thread
            TcpListener server = (TcpListener)ar.AsyncState;

            //Get and save the new client connection

            TcpClient newClient = server.EndAcceptTcpClient(ar);
            if (newClient != null)
            {
                tcpClients.Add(newClient);
                SaveNetworkError("LOG: Client succesfully conected.");
            }
            //Accept new connections asyncronously.
            tcpServer.BeginAcceptTcpClient(AcceptClient, server);

        }

        StreamWriter sw;

        /// <summary>
        /// To send the skeletons information by a broadcast udp message and to the tcp connections
        /// </summary>
        /// <param name="skeletons">Skeletons to send</param>
        public void Send(List<Body> skeletons)
        {

            //sw = new StreamWriter("KSTSkeletons (" + Environment.TickCount + ").txt", true);

            //No skeletons to send
            if (skeletons.Count == 0)
                return;

            //Check the FPS
            if (lastFrame + 1000.0f / FPS > Environment.TickCount)
                return;
            lastFrame = Environment.TickCount;

            //Encode the skeletons into the packet
            StringWriter packet = new StringWriter();
            EncodeSkeletons(skeletons, packet);
            byte[] bytes = Encoding.ASCII.GetBytes(packet.ToString());

            //**************************************
            //              TCP
            //**************************************

            //Encoding the tcp message adding boundaries ('B' at the beginnig and 'E' at the end of the message)
#if APPEND_OUTPUT
            var append = true;
#else
            var append = false;
#endif
            using (sw = new StreamWriter("KSTSkeletons.txt", append))
            {
                sw.WriteLine("TCP packet: " + 'B' + packet + 'E');
                sw.WriteLine();
            }

            byte[] tcpPacket = new byte[bytes.Length + 2];
            Array.Copy(bytes, 0, tcpPacket, 1, bytes.Length);
            tcpPacket[0] = (byte)'B';
            tcpPacket[tcpPacket.Length - 1] = (byte)'E';

            //List of clients closed
            List<TcpClient> oldClients = new List<TcpClient>();

            //Send the packet to the tcp clients
            for (int i = 0; i < tcpClients.Count; i++)
                try
                {
                    NetworkStream stream = tcpClients[i].GetStream();
                    stream.Write(tcpPacket, 0, tcpPacket.Length);
                }
                catch
                {
                    oldClients.Add(tcpClients[i]);
                }


            //Remove all clients closed
            foreach (var tcpClient in oldClients)
            {
                SaveNetworkError("LOG: Client DIsconnected");
                tcpClients.Remove(tcpClient);
                tcpClient.Close();
            }

#if SEND_UDP

            //**************************************
            //              UDP
            //**************************************

            //information to split the packet
            int subpacketSize = 250;
            string stringPacket = packet.ToString();
            int countOfSubPackets = (int)Math.Ceiling((float)stringPacket.Length / subpacketSize);
            string[] subpackets = new string[countOfSubPackets];
            byte[] udpPacket;

            //Create and send each subPacket
            for (int i = 0; i < subpackets.Length; i++)
            {
                //Broadcast information
                IPEndPoint endPoint = new IPEndPoint(IPAddress.Broadcast, UdpPort + i);

                subpackets[i] = idPacket + "-" + i + "-" + countOfSubPackets + ")" +
                    (i == subpackets.Length - 1 ? stringPacket.Substring(i * subpacketSize) : stringPacket.Substring(i * subpacketSize, subpacketSize));

                sw.WriteLine("UDP packet: " + subpackets[i]);
                sw.WriteLine();

                bytes = Encoding.ASCII.GetBytes(subpackets[i]);

                //Encoding the udp message in a 32-bit big-endian
                udpPacket = new byte[bytes.Length * 4];
                for (int k = 0; k < bytes.Length; k++)
                    udpPacket[4 * (k + 1) - 1] = bytes[k];

                //Send the udp broadcast message
                udpServer.Send(udpPacket, udpPacket.Length, endPoint);
            }
            idPacket++;
#endif
        }

        /// <summary>
        /// Encode the skeletons parameter and save it on the packet
        /// </summary>
        /// <param name="skeletons">skeletons to encode</param>
        /// <param name="packet">packet to save the skeleton</param>
        private void EncodeSkeletons(List<Body> skeletons, StringWriter packet)
        {
            //Encode the skeletons delimited by '/' character
            //EncodeSkeleton(skeletons[0], packet);
            // if (skeletons.Count > 1)
            for (int i = 0; i < skeletons.Count; i++)
            {
                if (i != 0)
                    packet.Write('/');
                if (skeletons[i].IsTracked)
                    EncodeSkeleton(skeletons[i], packet);
                //else
                //    EncodeSkeletonPoint(skeletons[i].position, packet);
            }
        }
        /// <summary>
        /// Encode the skeleton parameter and save it on the packet
        /// </summary>
        /// <param name="skeletons">skeleton to encode</param>
        /// <param name="packet">packet to save the skeleton</param>
        private void EncodeSkeleton(Body skeleton, StringWriter packet)
        {
            packet.Write("{0};", skeleton.TrackingId);
            EncodeSkeletonConfidence(skeleton, packet);
            //Encode the joints of this skeleton delimited by ';' character
            EncodeSkeletonJoint(skeleton, JointType.Head, packet);
            EncodeSkeletonJoint(skeleton, JointType.Neck, packet);
            EncodeSkeletonJoint(skeleton, JointType.HandTipLeft, packet);
            EncodeSkeletonJoint(skeleton, JointType.HandTipRight, packet);
            EncodeSkeletonJoint(skeleton, JointType.ThumbLeft, packet);
            EncodeSkeletonJoint(skeleton, JointType.ThumbRight, packet);
            EncodeSkeletonJoint(skeleton, JointType.HandLeft, packet);
            EncodeSkeletonJoint(skeleton, JointType.HandRight, packet);
            EncodeSkeletonJoint(skeleton, JointType.WristLeft, packet);
            EncodeSkeletonJoint(skeleton, JointType.WristRight, packet);
            EncodeSkeletonJoint(skeleton, JointType.ElbowLeft, packet);
            EncodeSkeletonJoint(skeleton, JointType.ElbowRight, packet);
            EncodeSkeletonJoint(skeleton, JointType.ShoulderLeft, packet);
            EncodeSkeletonJoint(skeleton, JointType.ShoulderRight, packet);
            EncodeSkeletonJoint(skeleton, JointType.SpineShoulder, packet);
            EncodeSkeletonJoint(skeleton, JointType.SpineMid, packet);
            EncodeSkeletonJoint(skeleton, JointType.SpineBase, packet);
            EncodeSkeletonJoint(skeleton, JointType.HipLeft, packet);
            EncodeSkeletonJoint(skeleton, JointType.HipRight, packet);
            EncodeSkeletonJoint(skeleton, JointType.KneeLeft, packet); ;
            EncodeSkeletonJoint(skeleton, JointType.KneeRight, packet);
            EncodeSkeletonJoint(skeleton, JointType.AnkleLeft, packet);
            EncodeSkeletonJoint(skeleton, JointType.AnkleRight, packet);
            EncodeSkeletonJoint(skeleton, JointType.FootLeft, packet);
            EncodeSkeletonJoint(skeleton, JointType.FootRight, packet);
            packet.Write("{0};", (int)skeleton.HandLeftState);
            packet.Write("{0}", (int)skeleton.HandRightState);

        }

        private void EncodeSkeletonConfidence(Body skeleton, StringWriter packet)
        {
            int sum = 0;
            foreach (var key in skeleton.Joints.Keys)
            {
                sum += (int)skeleton.Joints[key].TrackingState;
            }

            float confidence = sum / (2 * skeleton.Joints.Keys.Count());
            packet.Write(Math.Round(confidence, 3));
            packet.Write(';');

        }



        Dictionary<JointType, DateTime> lastGoodTrack = new Dictionary<JointType, DateTime>();



        /// <summary>
        /// Encode confidence in window time frame
        /// </summary>
        /// <param name="joint">Joint type to encode</param>
        /// <param name="packet">Writer</param>
        private void EncodeSkeletonConfidence(Joint joint, StringWriter packet)
        {
            if (joint.TrackingState == TrackingState.Tracked)
            {
                lastGoodTrack[joint.JointType] = DateTime.Now;
            }
            else if (!lastGoodTrack.Keys.Contains(joint.JointType))
            {
                lastGoodTrack[joint.JointType] = DateTime.Now - TimeSpan.FromHours(1);
            }

            int confidence = 0;
            int minInferredConfidence = 10;
            TimeSpan timeGap;
            switch (joint.TrackingState)
            {
                case TrackingState.NotTracked:
                    confidence = 0;
                    break;
                case TrackingState.Tracked:
                    confidence = 100;
                    break;
                case TrackingState.Inferred:
                    timeGap = DateTime.Now - lastGoodTrack[joint.JointType];
                    confidence = Math.Max(minInferredConfidence, 100 *  (ConfidenceWindow - (int)timeGap.TotalMilliseconds) / ConfidenceWindow);
                    break;
            }
            
            //Debug.Assert((100 >= confidence) && (minInferredConfidence <= confidence) || confidence == 0);

            //if (joint.JointType == JointType.AnkleRight)
            //{
            //    Debug.WriteLine(string.Format("Ankle right tracking state: {1}, confidence: {0}", confidence, joint.TrackingState));
            //}

            packet.Write(confidence);
        }




        /// <summary>
        /// Encode the joint parameter and save it on the packet
        /// </summary>
        /// <param name="skeletons">joint to encode</param>
        /// <param name="packet">packet to save the joint</param>
        private void EncodeSkeletonJoint(Body skeleton, JointType jointType, StringWriter packet)
        {
            EncodeSkeletonPoint(skeleton.Joints[jointType].Position, packet);
            packet.Write(',');
            EncodeSkeletonOrientation(skeleton.JointOrientations[jointType], packet);
            packet.Write(',');
            EncodeSkeletonConfidence(skeleton.Joints[jointType], packet);
            packet.Write(';');
        }

        /// <summary>
        ///  Encode the point parameter and save it on the packet
        /// </summary>
        /// <param name="jointOrientation">Vector4 in X,Y,Z,W order</param>
        /// <param name="packet">packet to save the Vector4</param>
        private void EncodeSkeletonOrientation(JointOrientation orientation, StringWriter packet)
        {
            packet.Write(Math.Round(orientation.Orientation.X, 3));
            packet.Write(',');
            packet.Write(Math.Round(orientation.Orientation.Y, 3));
            packet.Write(',');
            packet.Write(Math.Round(orientation.Orientation.Z, 3));
            packet.Write(',');
            packet.Write(Math.Round(orientation.Orientation.W, 3));
        }

        /// <summary>
        /// Encode the point parameter and save it on the packet
        /// </summary>
        /// <param name="skeletons">point to encode</param>
        /// <param name="packet">packet to save the point</param>
        private void EncodeSkeletonPoint(CameraSpacePoint point, StringWriter packet)
        {
            //Encode the point delimited by ',' character
            packet.Write(Math.Round(point.X, 3));
            packet.Write(',');
            packet.Write(Math.Round(point.Y, 3));
            packet.Write(',');
            packet.Write(Math.Round(point.Z, 3));
        }

        /// <summary>
        /// Saves the log in a file
        /// </summary>
        /// <param name="error">The description of the log</param>
        public void SaveNetworkError(string error)
        {
            DateTime d = DateTime.Now;
            string path = @"Reports\Report " + d.DayOfYear + "-" + d.Month + "-" + d.Year + ".txt";
            string s = "Time of day:" + d.TimeOfDay + "   " + error;

            lock (trafficLight)
            {
                if (File.Exists(path))
                {
                    StreamWriter sw = new StreamWriter(path, true, Encoding.ASCII);
                    sw.WriteLine(s);
                    sw.Close();
                }
                else
                {
                    StreamWriter sw = new StreamWriter(path, true, Encoding.ASCII);
                    sw.WriteLine("Report " + d.DayOfYear + "-" + d.Month + "-" + d.Year);
                    sw.WriteLine(s);
                    sw.Close();
                }
            }
        }

    }
}