﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Kinect;
using System.Net.Sockets;
using System.Net;
using System.IO;
using Timer = System.Timers.Timer;

namespace KSTServer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region General

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor sensor;
        private bool kinectLost;
        private System.Timers.Timer KinectValidationTimer;

        /// <summary>
        /// TCP and UDP Server
        /// </summary>
        private NetworkController server;

        private BodyFrameReader bodyFrameReader;
        private ColorFrameReader colorFrameReader;
        private int displayWidth;
        private int displayHeight;
        private Body[] bodies;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Pen used for drawing bones that are currently tracked
        /// </summary>
        private readonly Pen trackedBonePen = new Pen(Brushes.Green, 6);

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        private const double HandSize = 30;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as closed
        /// </summary>
        private readonly Brush handClosedBrush = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as opened
        /// </summary>
        private readonly Brush handOpenBrush = new SolidColorBrush(Color.FromArgb(128, 0, 255, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as in lasso (pointer) position
        /// </summary>
        private readonly Brush handLassoBrush = new SolidColorBrush(Color.FromArgb(128, 0, 0, 255));

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            this.sensor = KinectSensor.GetDefault();
            if (this.sensor != null)
            {
                this.sensor.Open();
                this.coordinateMapper = this.sensor.CoordinateMapper;
                FrameDescription colorFrameDescription = this.sensor.ColorFrameSource.FrameDescription;
                FrameDescription bodyFrameDescription = this.sensor.DepthFrameSource.FrameDescription;
                this.displayWidth = colorFrameDescription.Width;
                this.displayHeight = colorFrameDescription.Height;
                this.bodies = new Body[this.sensor.BodyFrameSource.BodyCount];
                this.colorFrameReader = this.sensor.ColorFrameSource.OpenReader();
                this.bodyFrameReader = this.sensor.BodyFrameSource.OpenReader();
            }

            InitializeComponent();

        }

        /// <summary>
        /// Execute startup tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            DirectoryInfo di = new DirectoryInfo(@"Reports");
            if (!di.Exists)
                di.Create();

            //Start Timer checking the kinect
            kinectLost = true;
            KinectValidationTimer = new Timer(1000);
            KinectValidationTimer.Elapsed += new ElapsedEventHandler(ValidateKinect);
            KinectValidationTimer.Start();

            //Load the settings and Start the servers
            Setting setting = Setting.LoadSettings("KSTServer.ini");
            server = new NetworkController(setting.UdpPort, setting.TcpPort, setting.ConfidenceWindow);
            server.FPS = setting.Fps;

            // Assign frame arrived events

            // Add an event handler to be called whenever there is new color frame data
            this.bodyFrameReader.FrameArrived += this.SensorSkeletonFrameReady;

            // Create the drawing group we'll use for drawing
            this.drawingGroup = new DrawingGroup();

            // Create an image source that we can use in our image control
            //this.imageSource = new DrawingImage(this.drawingGroup);
            //lock (ImageSkeleton)
            //{
            //    // Display the drawing using our image control
            //    ImageSkeleton.Source = this.imageSource;
            //}

            //**************************************
            //              Color
            //**************************************

            // Add an event handler to be called whenever there is new color frame data
            this.colorFrameReader.FrameArrived += this.SensorColorFrameReady;

        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Window_Closed(object sender, EventArgs e)
        {
            if (sensor != null)
                sensor.Close();
        }

        private void Config_Click(object sender, RoutedEventArgs e)
        {
            //Show the configuration window
            ConfigWindow configWindow = new ConfigWindow();
            configWindow.ShowDialog();

            //Load the settings
            Setting setting = Setting.LoadSettings("KSTServer.ini");

            //Restart the kinect with the new settings
            if (sensor != null)
            {
                sensor.Close();
                StartKinect(setting);

            }
            else
                SaveNetworkError("ERROR: Kinect sensor lost: Error Changing configuration");

            if (server != null)
            {
                if (server.TcpPort != setting.TcpPort || server.UdpPort != setting.UdpPort)
                {
                    server.Stop();
                    server = new NetworkController(setting.UdpPort, setting.TcpPort, setting.ConfidenceWindow);

                }
                server.FPS = setting.Fps;
            }
            SaveNetworkError("LOG: Settings Chaged");

        }

        private void StartKinect(Setting setting)
        {
            //**************************************
            //              Skeleton
            //**************************************

            // Turn on the skeleton stream to receive skeleton frames

#if SMOOTHING_ENABLED
            this.sensor.SkeletonStream.Enable(new TransformSmoothParameters()
    {
        Smoothing = setting.Smoothing,
        Correction = setting.Correction,
        Prediction = setting.Prediction,
        JitterRadius = setting.JitterRadius,
        MaxDeviationRadius = setting.MaxDeviationRadius
    }); 
#endif

            //**************************************
            //              Start the sensor!
            //**************************************
            try
            {
                this.sensor.Open();
#if ELEVATION_ENABLED
		                this.sensor.ElevationAngle = setting.SensorDegree;
#endif
            }
            catch
            {
                kinectLost = true;
                this.bodyFrameReader.FrameArrived -= SensorSkeletonFrameReady;
                this.colorFrameReader.FrameArrived -= SensorColorFrameReady;
                Action<bool> action = new Action<bool>(ChangeVisibility);
                Dispatcher.Invoke(action, true);
            }
        }

        #endregion

        #region ColorFrame

        /// <summary>
        /// Bitmap that will hold color information
        /// </summary>
        private WriteableBitmap colorBitmap;

        /// <summary>
        /// Event handler for Kinect sensor's ColorFrameReady event
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void SensorColorFrameReady(object sender, ColorFrameArrivedEventArgs e)
        {
            using (ColorFrame frame = e.FrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    using (frame)
                    {
                        FrameDescription frameDescription = frame.CreateFrameDescription(ColorImageFormat.Bgra);
                        int width = frameDescription.Width;
                        int height = frameDescription.Height;
                        uint bufferLength = (uint)(frameDescription.LengthInPixels * frameDescription.BytesPerPixel);
                        this.colorBitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgra32, null);
                        //this.ImageVideo.Source = this.colorBitmap;
                        this.ImageVideo.Source = new DrawingImage(this.drawingGroup);

                        colorBitmap.Lock();
                        frame.CopyConvertedFrameDataToIntPtr(colorBitmap.BackBuffer, bufferLength, ColorImageFormat.Bgra);
                        colorBitmap.AddDirtyRect(new Int32Rect(0, 0, width, height));
                        colorBitmap.Unlock();
                    }

                }
            }
        }

        #endregion

        #region SkeletonFrame

        /// <summary>
        /// Drawing group for skeleton rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Event handler for Kinect sensor's SkeletonFrameReady event
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void SensorSkeletonFrameReady(object sender, BodyFrameArrivedEventArgs e)
        {
            BodyFrameReference frameReference = e.FrameReference;
            // If nothing is tracked, null the joints reference

            BodyFrame frame = frameReference.AcquireFrame();

            if (frame != null)
            {
                // BodyFrame is IDisposable
                using (frame)
                {
                    using (DrawingContext dc = this.drawingGroup.Open())
                    {
                        // Draw a transparent background to set the render size

                        dc.DrawRectangle(Brushes.Transparent, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                        // draw the kinect bitmap if it's there
                        if (null != this.colorBitmap)
                        {
                            // determine the coordinates for displaying the image
                            Double w = colorBitmap.Width * this.displayHeight / colorBitmap.Height;
                            Double diffWidth = Math.Abs(this.displayWidth - w);
                            Double x = -(diffWidth / 2);
                            Double ww = w + x;
                            dc.DrawImage(colorBitmap, new Rect(x, 0.0, w, this.displayHeight));
                        }

                        // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                        // As long as those body objects are not disposed and not set to null in the array,
                        // those body objects will be re-used.
                        frame.GetAndRefreshBodyData(this.bodies);

                        foreach (Body body in this.bodies)
                        {
                            if (body.IsTracked)
                            {
                                var joints = body.Joints;

                                if (null != joints)
                                {
                                    // convert the joint points to depth (display) space
                                    Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();
                                    foreach (JointType jointType in joints.Keys)
                                    {
                                        ColorSpacePoint colorSpacePoint = this.coordinateMapper.MapCameraPointToColorSpace(joints[jointType].Position);
                                        jointPoints[jointType] = new Point(colorSpacePoint.X, colorSpacePoint.Y);
                                    }
                                    this.DrawBody(joints, jointPoints, dc);
                                    this.DrawHand(body.HandLeftState, jointPoints[JointType.HandLeft], dc);
                                    this.DrawHand(body.HandRightState, jointPoints[JointType.HandRight], dc);

                                }
                            }
                        }
                        // prevent drawing outside of our render area
                        this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                        //Send to TCP client and Udp broadcast the information of this frame
                        if (server != null && this.bodies.Length != 0)
                        {
                            try
                            {
                                server.Send(this.bodies.Where( x => x.IsTracked).ToList());
                            }
                            catch (InvalidOperationException ioe)
                            {
                                SaveNetworkError("ERROR: " + ioe.Message);
                                return;
                            }
                        }
                    }
                }
            }

        }

        #region Canvas Drawing Methods
        /// <summary>
        /// Draws a hand symbol if the hand is tracked: red circle = closed, green circle = opened; blue circle = lasso
        /// </summary>
        /// <param name="handState">state of the hand</param>
        /// <param name="handPosition">position of the hand</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawHand(HandState handState, Point handPosition, DrawingContext drawingContext)
        {
            switch (handState)
            {
                case HandState.Closed:
                    drawingContext.DrawEllipse(this.handClosedBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Open:
                    drawingContext.DrawEllipse(this.handOpenBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Lasso:
                    drawingContext.DrawEllipse(this.handLassoBrush, null, handPosition, HandSize, HandSize);


                    break;
            }
        }

        /// <summary>
        /// Draws one bone of a body (joint to joint)
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="jointType0">first joint of bone to draw</param>
        /// <param name="jointType1">second joint of bone to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBone(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, JointType jointType0, JointType jointType1, DrawingContext drawingContext)
        {
            Joint joint0 = joints[jointType0];
            Joint joint1 = joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == TrackingState.NotTracked ||
                joint1.TrackingState == TrackingState.NotTracked)
            {
                return;
            }

            // Don't draw if both points are inferred
            if (joint0.TrackingState == TrackingState.Inferred &&
                joint1.TrackingState == TrackingState.Inferred)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if ((joint0.TrackingState == TrackingState.Tracked) && (joint1.TrackingState == TrackingState.Tracked))
            {
                drawPen = this.trackedBonePen;
            }

            drawingContext.DrawLine(drawPen, jointPoints[jointType0], jointPoints[jointType1]);
        }

        /// <summary>
        /// Draws a body
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBody(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, DrawingContext drawingContext)
        {
            // Draw the bones

            // Torso
            this.DrawBone(joints, jointPoints, JointType.Head, JointType.Neck, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.Neck, JointType.SpineShoulder, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.SpineMid, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineMid, JointType.SpineBase, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.ShoulderRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.ShoulderLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineBase, JointType.HipRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineBase, JointType.HipLeft, drawingContext);

            // Right Arm    
            this.DrawBone(joints, jointPoints, JointType.ShoulderRight, JointType.ElbowRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.ElbowRight, JointType.WristRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristRight, JointType.HandRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.HandRight, JointType.HandTipRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristRight, JointType.ThumbRight, drawingContext);

            // Left Arm
            this.DrawBone(joints, jointPoints, JointType.ShoulderLeft, JointType.ElbowLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.ElbowLeft, JointType.WristLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristLeft, JointType.HandLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.HandLeft, JointType.HandTipLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristLeft, JointType.ThumbLeft, drawingContext);

            // Right Leg
            this.DrawBone(joints, jointPoints, JointType.HipRight, JointType.KneeRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.KneeRight, JointType.AnkleRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.AnkleRight, JointType.FootRight, drawingContext);

            // Left Leg
            this.DrawBone(joints, jointPoints, JointType.HipLeft, JointType.KneeLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.KneeLeft, JointType.AnkleLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.AnkleLeft, JointType.FootLeft, drawingContext);

            // Draw the joints
            foreach (JointType jointType in joints.Keys)
            {
                Brush drawBrush = null;

                TrackingState trackingState = joints[jointType].TrackingState;

                if (trackingState == TrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (trackingState == TrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
                }
            }
        }
        #endregion

        #endregion

        /// <summary>
        /// Method linked to timer.tick event. Control the kinect status of the server.  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValidateKinect(object sender, ElapsedEventArgs e)
        {
            if (!kinectLost)
            {
                try
                {
                    if (!sensor.IsAvailable)
                    {
                        SaveNetworkError(
                            "ERROR: KinectSensor unpluged, please solve this problem and start the application again.");
                        kinectLost = true;
                        this.bodyFrameReader.FrameArrived -= SensorSkeletonFrameReady;
                        this.colorFrameReader.FrameArrived -= SensorColorFrameReady;
                        Action<bool> action = new Action<bool>(ChangeVisibility);
                        Dispatcher.Invoke(action, true);
                    }
                }
                catch (Exception)
                {
                    SaveNetworkError(
                        "ERROR: KinectSensor unpluged, please solve this problem and start the application again.");
                    kinectLost = true;
                }
            }
            else
            {
                sensor = KinectSensor.GetDefault();
                //sensor = KinectSensor.KinectSensors.FirstOrDefault(s => s.Status == KinectStatus.Connected);
                if (sensor != null)
                {
                    Setting setting = Setting.LoadSettings("KSTServer.ini");
                    Action<Setting> action = new Action<Setting>(StartKinect);
                    Dispatcher.Invoke(action, setting);
                    kinectLost = false;
                    Action<bool> action1 = new Action<bool>(ChangeVisibility);
                    Dispatcher.Invoke(action1, false);
                    SaveNetworkError("LOG: Kinect error Recover succesfull.");
                }

            }
        }

        private void Report_Click(object sender, RoutedEventArgs e)
        {
            Report report = new Report();
            report.ShowDialog();
        }

        /// <summary>
        /// Saves the log in a file
        /// </summary>
        /// <param name="error">The description of the log</param>
        public void SaveNetworkError(string error)
        {
            DateTime d = DateTime.Now;
            string path = @"Reports\Report " + d.DayOfYear + "-" + d.Month + "-" + d.Year + ".txt";
            string s = "Time of day:" + d.TimeOfDay + "   " + error;


            if (File.Exists(path))
            {
                StreamWriter sw = new StreamWriter(path, true, Encoding.ASCII);
                sw.WriteLine(s);
                sw.Close();
            }
            else
            {
                StreamWriter sw = new StreamWriter(path, true, Encoding.ASCII);
                sw.WriteLine("Report " + d.DayOfYear + "-" + d.Month + "-" + d.Year);
                sw.WriteLine(s);
                sw.Close();
            }
        }

        public void ChangeVisibility(bool lost)
        {
            label1.Visibility = lost ? Visibility.Visible : Visibility.Hidden;
        }
    }
}