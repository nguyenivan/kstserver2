﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace KSTServer
{
    public class Setting
    {
        public int Fps { get; set; }
        public int UdpPort { get; set; }
        public int TcpPort { get; set; }
        public float Smoothing { get; set; }
        public float Correction { get; set; }
        public float Prediction { get; set; }
        public float JitterRadius { get; set; }
        public float MaxDeviationRadius { get; set; }
        public int SensorDegree { get; set; }
        public int ConfidenceWindow { get; set; }
        

        public Setting()
        {
            Fps = 60;
            UdpPort = 51000;
            TcpPort = 50000;
            Smoothing = 0.5f;
            Correction = 0.5f;
            Prediction = 0.5f;
            JitterRadius = 0.05f;
            MaxDeviationRadius = 0.04f;
            SensorDegree = 0;
            ConfidenceWindow = 1000;
            
        }

        /// <summary>
        /// Load the setting from the path file
        /// </summary>
        /// <returns>The setting loaded</returns>
        public static Setting LoadSettings(string path)
        {
            //Load a default setting
            Setting setting = new Setting();

            try
            {
                //Read the setting from the config file
                using (StreamReader reader = new StreamReader(path))
                {
                    while (!reader.EndOfStream)
                    {
                        string[] line = reader.ReadLine().Split(' ');

                        if (line.Length != 2)
                            continue;

                        if (line[0] == "FPS")
                            setting.Fps = int.Parse(line[1]);
                        else if (line[0] == "UDPPort")
                            setting.UdpPort = int.Parse(line[1]);
                        else if (line[0] == "TCPPort")
                            setting.TcpPort = int.Parse(line[1]);
                        else if (line[0] == "Smoothing")
                            setting.Smoothing = float.Parse(line[1]);
                        else if (line[0] == "Correction")
                            setting.Correction = float.Parse(line[1]);
                        else if (line[0] == "Prediction")
                            setting.Prediction = float.Parse(line[1]);
                        else if (line[0] == "JitterRadius")
                            setting.JitterRadius = float.Parse(line[1]);
                        else if (line[0] == "MaxDeviationRadius")
                            setting.MaxDeviationRadius = float.Parse(line[1]);
                        else if (line[0] == "KinectSensorDegree")
                            setting.SensorDegree = int.Parse(line[1]);
                        else if (line[0] == "ConfidenceWindow")
                            setting.ConfidenceWindow = int.Parse(line[1]);
                       
                    }
                }
            }
            catch
            {
                //If the config file has troubles then return a default setting
                setting = new Setting();
            }
            return setting;
        }

    }
}
