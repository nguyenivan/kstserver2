﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KSTServer
{
    /// <summary>
    /// Interaction logic for Report.xaml
    /// </summary>
    public partial class Report : Window
    {
        private string file;

        public Report()
        {
            InitializeComponent();

            file = @"Reports\Report ";
            DateTime c = DateTime.Now;
            calendar1.SelectedDate = c;
        }

        private void calendar1_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime d = DateTime.Now;
            listView1.Items.Clear();
            labelerror.Content = "";
            DateTime c;
            try
            {
                 c = calendar1.SelectedDate.Value;
            }
            catch
            {
                c = d;
            }

            if(c.Date <= d.Date)
            {
                string path = file + c.DayOfYear + "-" + c.Month + "-" + c.Year + ".txt";
                StreamReader r;
                try
                {
                    r = new StreamReader(path);
                }
                catch
                {
                    labelerror.Content = "No exist report of erros in that especific date";
                    return;
                }

                string read = r.ReadLine();
                while(read != null)
                {
                    listView1.Items.Add(read);
                    read = r.ReadLine();
                }
            }
            else
            {
                labelerror.Content = "No exist report of erros in that especific date";
            }
            
            
        }
    }
}
